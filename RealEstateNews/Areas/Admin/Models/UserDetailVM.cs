﻿using Microsoft.AspNetCore.Mvc.Rendering;
using RealEstateNews.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstateNews.Areas.Admin.Models
{
    public class UserDetailVM : RegisterVM
    {
        public EnumRole Role { get; set; }
        public SelectList Roles => new SelectList(Enum.GetValues(typeof(EnumRole)).Cast<EnumRole>().Select(t => t.ToString()));
    }
}
