﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstateNews.Areas.Admin.Models
{
    public class DashboardVM
    {
        public int NumberOfUsers { get; set; }
        public int NumberOfPosts { get; set; }
        public int NumberOfCategories { get; set; }
    }
}
