﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RealEstateNews.Areas.Admin.Models;
using RealEstateNews.Entities;
using RealEstateNews.Models;
using RealEstateNews.Repositories;

namespace RealEstateNews.Areas.Admin.Controllers
{
    public class DashboardController : AdminBaseController
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly RealEstateNewsContext _context;
        private readonly IMapper _mapper;

        public DashboardController(RealEstateNewsContext context, RoleManager<IdentityRole> roleManager, SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, IMapper mapper)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            var vm = new DashboardVM
            {
                NumberOfUsers = _context.Users.Count(),
                NumberOfPosts = _context.Posts.Count(),
                NumberOfCategories = _context.Categories.Count()
            };
            return View(vm);
        }

        public IActionResult AddUser()
        {
            return PartialView("_UserDetail", new UserDetailVM());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUser(UserDetailVM model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                user = new IdentityUser { UserName = model.UserName };
                var userResult = await _userManager.CreateAsync(user, model.Password);
                if (userResult.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, model.Role.ToString());
                }
            }

            TempData["Success"] = "Create new user successfully!";
            return new JsonResult(null);
        }

        public IActionResult AddCategory()
        {
            return PartialView("_CategoryDetail");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddCategory(CategoryVM model)
        {
            var category = _mapper.Map<Category>(model);
            _context.Categories.Add(category);
            await _context.SaveChangesAsync();

            TempData["Success"] = "Create new category successfully!";
            return new JsonResult(null);
        }

        public IActionResult CreatePost()
        {
            var postVM = new PostVM
            {
                Categories = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_context.Categories.Select(i => new { Text = i.Name, Value = i.Id}), "Value", "Text")
            };
            return View("PostDetail", postVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreatePost(PostVM model)
        {
            if (!ModelState.IsValid)
            {
                return View("PostDetail", model);
            }

            Post post = new Post();
            if(model.Id > 0)
            {
                post = _context.Posts.First(p => p.Id == model.Id);
            }
            _mapper.Map(model, post);
            if (post.Id == 0)
            {
                post.CreatedDate = DateTime.Now;
                post.CreatedBy = _userManager.GetUserId(User);
            }
            else
            {
                post.UpdatedDate = DateTime.Now;
                post.UpdatedBy = _userManager.GetUserId(User);
            }

            if(model.CoverImage!= null && model.CoverImage.Length > 0)
            {
                string WebRoot = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\images"}";
                var splits = Path.GetFileName(model.CoverImage.FileName).Split('.');
                var extension = splits[splits.Length - 1].ToLower();
                var fileName = $"{Guid.NewGuid()}.{extension}";

                using (var stream = new FileStream(Path.Combine(WebRoot, fileName), FileMode.Create))
                {
                    await model.CoverImage.CopyToAsync(stream);
                }
                post.ImageUrl = fileName;
            }

            _context.Entry(post).State = post.Id == 0 ? Microsoft.EntityFrameworkCore.EntityState.Added : Microsoft.EntityFrameworkCore.EntityState.Modified;
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        public IActionResult Posts()
        {
            var posts = _context.Posts.OrderByDescending(x => x.CreatedDate).Select(p => _mapper.Map<PostVM>(p)).ToList();
            return View(posts);
        }

        public IActionResult EditPost(int id)
        {
            var postVM = _context.Posts.Where(p => p.Id == id).Select(p => _mapper.Map<PostVM>(p)).FirstOrDefault();
            postVM.Categories = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_context.Categories.Select(i => new { Text = i.Name, Value = i.Id }), "Value", "Text");
            return View("PostDetail", postVM);
        }
    }
}