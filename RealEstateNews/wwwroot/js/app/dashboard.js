﻿var DashboardController = function () {

    function submitForm(e) {
        e.preventDefault();
        var $modalDialog = $("#modalDialog");
        var $form = $modalDialog.find('form');
        if (!$form.valid()) {
            e.preventDefault();
            return false;
        }
        var actionUrl = $form.attr('action');
        var data = $form.serialize();

        $.post(actionUrl, data).done(function (data) {
            window.location.reload();
        });
    }


    var initPostDetail = function () {
        $("#CoverImage").on('change', function () {
            var fileName = $(this).val();
            $(this).next().html(fileName);
        })
    }

    var initIndex = function () {
        var $modalDialog = $("#modalDialog");
        var bgColorHeader = "";
        var textColorHeader = "";
        $('a[data-toggle="modal"]').click(function (e) {
            var $this = $(this);
            var url = $this.data('url');
            bgColorHeader = $this.data('bgcolor');
            textColorHeader = $this.data('textcolor');
            $.get(url).done(function (data) {
                $modalDialog.find(".modal-title").html($this.data('title'));
                $modalDialog.find('.modal-body').html(data);
                if (bgColorHeader !== "") {
                    $modalDialog.find('.modal-header').addClass(bgColorHeader);
                }
                if (textColorHeader !== "") {
                    $modalDialog.find('.modal-header').addClass(textColorHeader);
                }                
                $modalDialog.modal('show');
            });
        });

        $modalDialog.on('submit', 'form', function (e) {
            submitForm(e);
        })
        $modalDialog.on('click', '[data-save="modal"]', function (e) {
            submitForm(e);
        });

        $modalDialog.on('hidden.bs.modal', function (e) {
            if (bgColorHeader !== "") {
                $modalDialog.find('.modal-header').toggleClass(bgColorHeader);
                bgColorHeader = "";
            }
            if (textColorHeader !== "") {
                $modalDialog.find('.modal-header').toggleClass(textColorHeader);
                textColorHeader = "";
            }            
        });
    };

    return {
        initIndex: initIndex,
        initPostDetail: initPostDetail
    }
}();