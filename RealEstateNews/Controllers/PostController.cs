﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RealEstateNews.Models;
using RealEstateNews.Repositories;

namespace RealEstateNews.Controllers
{
    public class PostController : Controller
    {
        private readonly RealEstateNewsContext _context;
        private readonly IMapper _mapper;

        public PostController(RealEstateNewsContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IActionResult Index(int id)
        {
            var post = _context.Posts.Where(p => p.Id == id).Select(t => _mapper.Map<PostVM>(t)).FirstOrDefault();
            return View(post);
        }

        public IActionResult Categories()
        {
            return PartialView("_Categories", _context.Categories.Select(c => _mapper.Map<CategoryVM>(c)).ToList());
        }

        public IActionResult Category(int id)
        {
            return View();
        }
    }
}