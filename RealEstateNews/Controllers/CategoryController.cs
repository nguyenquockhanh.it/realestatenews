﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RealEstateNews.Models;
using RealEstateNews.Repositories;

namespace RealEstateNews.Controllers
{
    public class CategoryController : Controller
    {
        private readonly RealEstateNewsContext _context;
        private readonly IMapper _mapper;

        public CategoryController(RealEstateNewsContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IActionResult Index(int id)
        {
            var posts = _context.Posts.Where(i => i.CategoryId == id).Select(p => _mapper.Map<PostVM>(p)).ToList();
            return View(posts);
        }
    }
}