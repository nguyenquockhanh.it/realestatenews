﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstateNews.TagHelpers
{
    [HtmlTargetElement("a", Attributes = "a-dialog")]
    public class AnchorDialogTagHelper : TagHelper
    {
        public string ButtonStyle { get; set; }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            base.Process(context, output);

            output.Attributes.SetAttribute("href", "javascript:();");
            output.Attributes.SetAttribute("data-toggle", "modal");
            output.Attributes.SetAttribute("data-bgcolor", $"bg-{ButtonStyle}");
            output.Attributes.SetAttribute("class", $"btn btn-block btn-{ButtonStyle}");            
        }
    }
}
