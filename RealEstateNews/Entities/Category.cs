﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstateNews.Entities
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }
        public IList<Post> Posts { get; set; }
    }
}
