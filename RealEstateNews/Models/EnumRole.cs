﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstateNews.Models
{
    public enum EnumRole
    {
        Admin,
        Moderator,
        User
    }
}
