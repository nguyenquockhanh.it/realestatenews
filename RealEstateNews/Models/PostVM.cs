﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstateNews.Models
{
    public class PostVM
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        
        public string ImageUrl { get; set; }

        [DisplayName("Category")]
        public int CategoryId { get; set; }
        [Required]
        public string Body { get; set; }
        public SelectList Categories;

        
        public IFormFile CoverImage { get; set; }
        public bool IsHighlight { get; set; }        
    }
}
