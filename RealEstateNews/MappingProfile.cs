﻿using AutoMapper;
using RealEstateNews.Entities;
using RealEstateNews.Models;

namespace RealEstateNews
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CategoryVM, Category>().ReverseMap();
            CreateMap<Post, PostVM>();
            CreateMap<PostVM, Post>().ForMember(x => x.ImageUrl, opts => opts.Ignore());
        }
    }
}
