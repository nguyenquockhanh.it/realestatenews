﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RealEstateNews.Extensions
{
    public static class Extensions
    {
        public static string SubStringUntilLastWord(this string str, int length)
        {
            var subStr = str.Substring(0, length);
            var indexOfSpace = str.IndexOf(' ', length);
            if (length < indexOfSpace)
            {
                subStr += str.Substring(length, indexOfSpace - length);
            }

            return subStr;
        }
    }

    public static class Slug
    {
        public static string GenerateSlug(this string phrase)
        {
            string str = phrase.RemoveDiacritics().ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 200 ? str.Length : 200).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        public static string RemoveDiacritics(this string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            var normalised = value.Normalize(NormalizationForm.FormD).ToCharArray();

            return new string(normalised.Where(c => (int)c <= 127).ToArray());
        }
    }
}
