﻿using Microsoft.AspNetCore.Identity;
using RealEstateNews.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstateNews.Repositories
{
    public class DbInitializer
    {
        public static async Task InitilizeAsync(RealEstateNewsContext context, RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            string[] roleNames = Enum.GetValues(typeof(EnumRole)).Cast<EnumRole>().Select(r => r.ToString()).ToArray();
            IdentityResult roleResult;
            foreach (var roleName in roleNames)
            {
                var roleExist = await roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    roleResult = await roleManager.CreateAsync(new IdentityRole { Name = roleName, NormalizedName = roleName.ToUpper() });
                }
            }

            var adminUserExist = await userManager.FindByNameAsync("admin");
            if (adminUserExist == null)
            {
                var user = new IdentityUser { UserName = "admin" };
                var userResult = await userManager.CreateAsync(user, "123456");
                if (userResult.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, EnumRole.Admin.ToString());
                }
            }
        }
    }
}
