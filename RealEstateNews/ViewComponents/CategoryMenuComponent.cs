﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RealEstateNews.Models;
using RealEstateNews.Repositories;
using System.Linq;

namespace RealEstateNews.ViewComponents
{
    public class CategoryMenuComponent : ViewComponent
    {
        private readonly RealEstateNewsContext _context;
        private readonly IMapper _mapper;
        public CategoryMenuComponent(RealEstateNewsContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IViewComponentResult Invoke()
        {
            var categories = _context.Categories.Select(c => _mapper.Map<CategoryVM>(c)).ToList();
            return View(categories);
        }
    }
}
