﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RealEstateNews.Models;
using RealEstateNews.Repositories;
using System.Linq;

namespace RealEstateNews.ViewComponents
{
    public class HighlightPostsComponent : ViewComponent
    {
        private readonly RealEstateNewsContext _context;
        private readonly IMapper _mapper;
        public HighlightPostsComponent(RealEstateNewsContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IViewComponentResult Invoke()
        {
            var highlightPosts = _context.Posts.Where(i => i.IsHighlight).OrderByDescending(i => i.CreatedDate).Take(3).Select(p => _mapper.Map<PostVM>(p));
            return View(highlightPosts);
        }
    }
}
