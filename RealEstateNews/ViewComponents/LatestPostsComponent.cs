﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RealEstateNews.Models;
using RealEstateNews.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstateNews.ViewComponents
{
    public class LatestPostsComponent : ViewComponent
    {
        private readonly RealEstateNewsContext _context;
        private readonly IMapper _mapper;
        public LatestPostsComponent(RealEstateNewsContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IViewComponentResult Invoke()
        {
            var highlightPosts = _context.Posts.Where(i => !i.IsHighlight).OrderByDescending(i => i.CreatedDate).Select(p => _mapper.Map<PostVM>(p)).ToList();
            return View(highlightPosts);
        }
    }
}
