RealEstateNews is CMS to manage news about real estate using Asp.Net Core platform


# Features!
    Admin Dashboard
  - Create User
  - Create Category
  - Create and edit Posts

    Homepage    
  - Show List of Posts
  - Slug Route for friendly url
  - Show List of Posts by Category
  - About Us(Using Razor Page)
  - Login , Logout and Register

### How to use
Access this site with url: https://sd0773.azurewebsites.net/

To login as Admin role, You can use account:

username: admin

password: 123456